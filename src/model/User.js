export class User {
    static counter = 0
    id;
    vorname;
    nachname;
    beschreibung;
    date;

    constructor(vorname, nachname, beschreibung) {
        this.id = User.counter++
        this.vorname = vorname
        this.nachname = nachname
        this.beschreibung = beschreibung
        this.date = new Date()
    }
}
